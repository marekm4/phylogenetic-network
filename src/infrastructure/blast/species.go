package blast

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"net/url"
	"regexp"
	"strconv"
	"strings"
	"time"

	"gitlab.com/marekm4/phylogenetic-network/src/application/serach"
)

func FindSpecies(sequence string) (string, int, error) {
	data := url.Values{}
	data.Set("CMD", "Put")
	data.Set("PROGRAM", "blastn")
	data.Set("DATABASE", "nt")
	data.Set("QUERY", sequence)

	log.Println("Search for sequence", sequence)
	resp, err := http.PostForm(`https://blast.ncbi.nlm.nih.gov/blast/Blast.cgi`, data)
	if err != nil {
		return "", 0, err
	}
	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return "", 0, err
	}

	re := regexp.MustCompile(`RID = (.*)`)
	submatches := re.FindSubmatch(body)

	if len(submatches) < 2 {
		log.Println("Search for sequence", sequence, "does not return id")
		return "", 0, serach.ErrSpeciesNotFound
	}

	id := string(submatches[1])

	re = regexp.MustCompile(`RTOE = (.*)`)
	submatches = re.FindSubmatch(body)

	if len(submatches) < 2 {
		log.Println("Search for sequence", sequence, "does not return wait time")
		return "", 0, serach.ErrSpeciesNotFound
	}

	wait, err := strconv.Atoi(string(submatches[1]))
	if err != nil {
		return "", 0, err
	}
	log.Println("Search for sequence", sequence, "found with result", id, "and wait time of", wait)

	return id, wait, nil
}

type SpeciesQueryResult struct {
	BlastOutput2 []struct {
		Report struct {
			Results struct {
				Search struct {
					Hits []struct {
						Description []struct {
							Sciname string `json:sciname`
						} `json:description`
					} `json:hits`
				} `json:search`
			} `json:results`
		} `json:report`
	}
}

func SequenceToSpecies(sequence string) ([]string, error) {
	id, wait, err := FindSpecies(sequence)
	if err != nil {
		return []string{}, err
	}

	log.Println("Waiting", wait, "seconds for results for search", id)
	time.Sleep(time.Duration(wait) * time.Second)

	for {
		log.Println("Waiting 5 seconds for results for search", id)
		time.Sleep(5 * time.Second)

		log.Println("Get results status for search", id)
		resp, err := http.Get(fmt.Sprintf(`https://blast.ncbi.nlm.nih.gov/blast/Blast.cgi?CMD=Get&FORMAT_OBJECT=SearchInfo&RID=%s`, id))
		if err != nil {
			return []string{}, err
		}
		defer resp.Body.Close()

		body, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			return []string{}, err
		}
		content := string(body)

		if strings.Contains(content, "Status=WAITING") {
			log.Println("Search", id, "is processing")
			continue
		}

		if strings.Contains(content, "Status=FAILED") || strings.Contains(content, "Status=UNKNOWN") {
			log.Println("Search", id, "failed")
			return []string{}, serach.ErrSpeciesNotFound
		}

		if strings.Contains(content, "Status=READY") {
			log.Println("Search", id, "is ready")
			break
		}
	}

	log.Println("Get results for search", id)
	resp, err := http.Get(fmt.Sprintf(`https://blast.ncbi.nlm.nih.gov/blast/Blast.cgi?RESULTS_FILE=on&RID=%s&FORMAT_TYPE=JSON2_S&FORMAT_OBJECT=Alignment&CMD=Get`, id))
	if err != nil {
		return []string{}, err
	}
	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return []string{}, err
	}

	result := SpeciesQueryResult{}
	err = json.Unmarshal(body, &result)
	if err != nil {
		return []string{}, err
	}

	species := []string{}
	speciesExists := map[string]struct{}{}

	for _, b := range result.BlastOutput2 {
		for _, h := range b.Report.Results.Search.Hits {
			for _, d := range h.Description {
				if _, ok := speciesExists[d.Sciname]; !ok {
					speciesExists[d.Sciname] = struct{}{}
					species = append(species, d.Sciname)
				}
			}
		}
	}

	log.Println("Species for search", id, "found with result", species)
	return species, nil
}
