package wikipedia

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"regexp"

	"gitlab.com/marekm4/phylogenetic-network/src/application/serach"
)

type ParseQueryResult struct {
	Parse struct {
		Text struct {
			Content string `json:"*"`
		} `json:text`
	} `json:parse`
}

func ImageFromPageID(pageID int) (string, error) {
	log.Println("Search image for page", pageID)
	resp, err := http.Get(fmt.Sprintf(`https://en.wikipedia.org/w/api.php?action=parse&format=json&pageid=%d`, pageID))
	if err != nil {
		return "", err
	}
	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return "", err
	}

	result := ParseQueryResult{}
	err = json.Unmarshal(body, &result)
	if err != nil {
		return "", err
	}

	re := regexp.MustCompile(`src="([^"]*)"`)
	submatches := re.FindStringSubmatch(result.Parse.Text.Content)

	if len(submatches) < 2 {
		log.Println("Image for page", pageID, "not found")
		return "", serach.ErrImageNotFound
	}

	url := "https:" + submatches[1]
	log.Println("Image for page", pageID, "found with result", url)
	return url, nil
}

func NameToImage(name string) (url string, err error) {
	pageID, err := FindPageID(name)
	if err != nil {
		return "", err
	}

	imageURL, err := ImageFromPageID(pageID)
	if err != nil {
		return "", err
	}

	return imageURL, err
}
