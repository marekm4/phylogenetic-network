package wikipedia

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestFindPageID(t *testing.T) {
	pageID, err := FindPageID("Homo sapiens")
	assert.NoError(t, err)
	assert.Equal(t, 99645, pageID)
}
