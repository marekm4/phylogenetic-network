package wikipedia

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestImageFromPageID(t *testing.T) {
	imageURL, err := ImageFromPageID(99645)
	assert.NoError(t, err)
	assert.Equal(t, "https://upload.wikimedia.org/wikipedia/commons/thumb/6/68/Akha_cropped_hires.JPG/200px-Akha_cropped_hires.JPG", imageURL)
}

func TestNameToImage(t *testing.T) {
	imageURL, err := NameToImage("Homo sapiens")
	assert.NoError(t, err)
	assert.Equal(t, "https://upload.wikimedia.org/wikipedia/commons/thumb/6/68/Akha_cropped_hires.JPG/200px-Akha_cropped_hires.JPG", imageURL)
}
