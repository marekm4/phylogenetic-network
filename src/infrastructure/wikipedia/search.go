package wikipedia

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"net/url"

	"gitlab.com/marekm4/phylogenetic-network/src/application/serach"
)

type SearchQueryResult struct {
	Query struct {
		Search []struct {
			PageID int `json:pageid`
		} `json:search`
	} `json:query`
}

func FindPageID(query string) (int, error) {
	log.Println("Search page for", query)
	resp, err := http.Get(fmt.Sprintf(`https://en.wikipedia.org/w/api.php?action=query&format=json&list=search&utf8=1&srsearch=%s`, url.QueryEscape(query)))
	if err != nil {
		return 0, err
	}
	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return 0, err
	}

	result := SearchQueryResult{}
	err = json.Unmarshal(body, &result)
	if err != nil {
		return 0, err
	}

	if len(result.Query.Search) == 0 {
		log.Println("Page for", query, "not found with result")
		return 0, serach.ErrImageNotFound
	}

	pageID := result.Query.Search[0].PageID
	log.Println("Page for", query, "found with result", pageID)
	return pageID, nil
}
