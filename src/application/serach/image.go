package serach

import "errors"

var ErrImageNotFound = errors.New("image not found")

type SpeciesToImage = func(species string) (url string, err error)
