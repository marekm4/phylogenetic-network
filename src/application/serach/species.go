package serach

import "errors"

var ErrSpeciesNotFound = errors.New("species not found")

type SequenceToSpecies = func(sequence string) (species []string, err error)
