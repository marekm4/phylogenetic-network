package domain

type SpiecesID int

type Spieces struct {
	id    SpiecesID
	name  string
	image string
}

func NewSpieces(name string) *Spieces {
	return &Spieces{name: name}
}

func (s *Spieces) setID(id SpiecesID) {
	s.id = id
}

func (s *Spieces) setImage(image string) {
	s.image = image
}

func (s *Spieces) ID() SpiecesID {
	return s.id
}

func (s *Spieces) Name() string {
	return s.name
}

func (s *Spieces) Image() string {
	return s.image
}
