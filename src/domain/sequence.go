package domain

type SequenceID int

type Sequence struct {
	id       SequenceID
	name     string
	sequence string
}

func NewSequence(name, sequence string) *Sequence {
	return &Sequence{name: name, sequence: sequence}
}

func (s *Sequence) setID(id SequenceID) {
	s.id = id
}

func (s *Sequence) ID() SequenceID {
	return s.id
}

func (s *Sequence) Name() string {
	return s.name
}

func (s *Sequence) Sequence() string {
	return s.sequence
}
