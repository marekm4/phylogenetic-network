package domain

type AlignmentID int

type Alignment struct {
	id       AlignmentID
	sequence Sequence
	species  Spieces
	cover    float64
}

func NewAlignment(sequence Sequence, species Spieces, cover float64) *Alignment {
	return &Alignment{sequence: sequence, species: species, cover: cover}
}

func (a *Alignment) setID(id AlignmentID) {
	a.id = id
}

func (a *Alignment) ID() AlignmentID {
	return a.id
}

func (a *Alignment) Sequence() Sequence {
	return a.sequence
}

func (a *Alignment) Species() Spieces {
	return a.species
}

func (a *Alignment) Cover() float64 {
	return a.cover
}
