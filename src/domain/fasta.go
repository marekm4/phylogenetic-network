package domain

import (
	"errors"
	"strings"
)

var ErrEmptySequence = errors.New("empty sequence")

func ParseFasta(fasta string) (string, string, error) {
	fasta = strings.TrimSpace(fasta)

	if len(fasta) == 0 {
		return "", "", ErrEmptySequence
	}

	name := ""
	sequence := ""

	lines := strings.Split(strings.Replace(fasta, "\r\n", "\n", -1), "\n")
	for _, line := range lines {
		line = strings.TrimSpace(line)
		if len(line) > 0 {
			if line[0] == '>' {
				name += line[1:]
			} else {
				if len(name) == 0 {
					name = line
				}
				sequence += line
			}
		}
	}

	return name, sequence, nil
}
