package domain

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestParseFasta(t *testing.T) {
	testCases := map[string]struct {
		fasta            string
		expectedName     string
		expectedSequence string
		expectedError    error
	}{
		"empty": {
			fasta:         "",
			expectedError: ErrEmptySequence,
		},
		"one line, sequence only": {
			fasta:            "ABCD",
			expectedName:     "ABCD",
			expectedSequence: "ABCD",
		},
		"multiline, sequence only": {
			fasta:            "ABCD\nDFGH",
			expectedName:     "ABCD",
			expectedSequence: "ABCDDFGH",
		},
		"multiline": {
			fasta:            ">Name\nABCD\nDFGH",
			expectedName:     "Name",
			expectedSequence: "ABCDDFGH",
		},
		"real example": {
			fasta: `>NC_015439.3:c41667474-41661660 Solanum lycopersicum cultivar Heinz 1706 chromosome 2, SL3.0, whole genome shotgun sequence
					AAGAAATGAGTGATCATCCATCCTCTCATTAGATAATTCCCAATTTAACCATCCTTCTATATAGTGCTTA
					TTCAAAACAAAGATAACATAGGAATAAACAGACAATGAAAAATCTTAGTTTAGATAATTAGTAGTAGTAA`,
			expectedName:     "NC_015439.3:c41667474-41661660 Solanum lycopersicum cultivar Heinz 1706 chromosome 2, SL3.0, whole genome shotgun sequence",
			expectedSequence: "AAGAAATGAGTGATCATCCATCCTCTCATTAGATAATTCCCAATTTAACCATCCTTCTATATAGTGCTTATTCAAAACAAAGATAACATAGGAATAAACAGACAATGAAAAATCTTAGTTTAGATAATTAGTAGTAGTAA",
		},
	}

	for n, tc := range testCases {
		t.Run(n, func(t *testing.T) {
			name, sequence, err := ParseFasta(tc.fasta)
			if tc.expectedError == nil {
				assert.NoError(t, err)
				assert.Equal(t, tc.expectedName, name)
				assert.Equal(t, tc.expectedSequence, sequence)
			} else {
				assert.Error(t, err)
				assert.EqualError(t, err, ErrEmptySequence.Error())
			}
		})
	}
}
