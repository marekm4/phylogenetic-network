package main

import (
	"fmt"
	"io/ioutil"
	"os"

	"gitlab.com/marekm4/phylogenetic-network/src/infrastructure/blast"
	"gitlab.com/marekm4/phylogenetic-network/src/infrastructure/wikipedia"
)

func main() {
	data, err := ioutil.ReadAll(os.Stdin)
	if err != nil {
		panic(err)
	}
	species, err := blast.SequenceToSpecies(string(data))
	if err != nil {
		fmt.Println(err)
	}
	for _, s := range species {
		url, err := wikipedia.NameToImage(s)
		if err != nil {
			fmt.Println(err)
		}
		fmt.Println(url)
	}
}
